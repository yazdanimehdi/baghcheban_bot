from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class User(models.Model):
    chat_id = models.BigIntegerField(default=0)
    phone = models.CharField(max_length=11)
    name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.phone


class Experts(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=11)
    telegram_id = models.CharField(max_length=100)

    def __str__(self):
        return self.name


def upload_location(instance, filename):
    return "%s/%s" % (instance.name, filename)


class Seed(models.Model):
    name = models.CharField(max_length=100)
    price = models.BigIntegerField()
    image = models.ImageField(upload_to=upload_location,
                              width_field='width', height_field='height')
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    availability = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Flower(models.Model):
    name = models.CharField(max_length=100)
    price = models.BigIntegerField()
    image = models.ImageField(upload_to=upload_location,
                              width_field='width', height_field='height')
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    availability = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Utilities(models.Model):
    name = models.CharField(max_length=100)
    price = models.BigIntegerField()
    image = models.ImageField(upload_to=upload_location,
                              width_field='width', height_field='height')
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    availability = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Fertilizer(models.Model):
    name = models.CharField(max_length=100)
    price = models.BigIntegerField()
    image = models.ImageField(upload_to=upload_location,
                              width_field='width', height_field='height')
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    availability = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    item = GenericForeignKey()
    quantity = models.IntegerField(default=0)
    invoice = models.ForeignKey(to='Invoice', on_delete=models.CASCADE)

    def __str__(self):
        return self.item.name


class Invoice(models.Model):
    user = models.ForeignKey(to='User', on_delete=models.CASCADE)
    invoice_number = models.DateTimeField(auto_now=True)
    address = models.CharField(max_length=300, default='0')
    amount = models.IntegerField(default=0)
    is_active = models.BooleanField(default=False)
    is_paid = models.BooleanField(default=False)
    is_done = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user.name)


class InvoicePendingPayment(models.Model):
    invoice = models.ForeignKey(to='Invoice', on_delete=models.CASCADE)
    discount_code = models.CharField(max_length=20)
    code = models.CharField(max_length=20)
    is_active = models.BooleanField(default=False)
    amount = models.IntegerField()


