import os
from enum import IntEnum

import django
import telegram
from telegram.ext import ConversationHandler


class BotStateService(IntEnum):
    GETPHONE = 1
    SELECTSERVICE = 2
    ORDER = 3
    EDIT = 5
    CONFIRM = 6
    PAYMENT = 7
    PAYMENT_CONFIRM = 8
    NAME = 9


def start(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from baghcheban.models import User
    user = User.objects.filter(chat_id=update.message.chat_id)
    if user:
        user_data['user'] = user[0]
        keyboard = [[telegram.InlineKeyboardButton(text='مشاوره‌ي آنلاین گل و گیاه', callback_data=1)],
                    [telegram.InlineKeyboardButton(text='فروشگاه', callback_data=2)]]
        inline_keyboard = telegram.InlineKeyboardMarkup(keyboard, resize_keyboard=True)
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="*لطفا خدمت مورد نظرتان رو انتخاب کنید:*",
                        reply_markup=inline_keyboard,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return BotStateService.SELECTSERVICE
    else:
        reply_markup = telegram.ReplyKeyboardMarkup(
            [[telegram.KeyboardButton('شمارتو به اشتراک بذار', request_contact=True)]], one_time_keyboard=True)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="*سلام\n"
                             "برای استفاده از خدمات ما از طریق گزینه‌ی زیر شماریتان را با ما به اشتراک بگذارید*",
                        reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        return BotStateService.GETPHONE


def get_phone(bot, update, user_data):
    if update.message.contact.phone_number[0] == '+':
        phone = '0' + update.message.contact.phone_number[3:]
    else:
        phone = '0' + update.message.contact.phone_number[2:]
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from baghcheban.models import User
    chat_id = update.message.chat_id
    user = User()
    user.phone = phone
    user.chat_id = chat_id
    user.save()
    user_data['user'] = user
    keyboard = [[telegram.InlineKeyboardButton(text='مشاوره‌ي آنلاین گل و گیاه', callback_data='1')],
                [telegram.InlineKeyboardButton(text='فروشگاه', callback_data='2')]]
    inline_keyboard = telegram.InlineKeyboardMarkup(keyboard, resize_keyboard=True)
    bot.sendMessage(chat_id=user_data['user'].chat_id,
                    text="*لطفا خدمت مورد نظرتان رو انتخاب کنید:*",
                    reply_markup=inline_keyboard,
                    parse_mode=telegram.ParseMode.MARKDOWN)
    return BotStateService.SELECTSERVICE


def select_type(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from baghcheban.models import Experts
    query = update.callback_query
    if query['data'] == '2':
        inline_keyboard_food = telegram.InlineKeyboardMarkup(
            [[telegram.InlineKeyboardButton(text='خرید گل و گیاه', callback_data='Flower')],
             [telegram.InlineKeyboardButton(text='خرید کود و سم', callback_data='Fertilizer')],
             [telegram.InlineKeyboardButton(text='خرید بذر', callback_data='Seed')],
             [telegram.InlineKeyboardButton(text='خرید لوازم و تجهیزات', callback_data='Utilities')]])
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="*لطفا دسته بندی مورد نظر را انتخاب کنید*",
                        reply_markup=inline_keyboard_food,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return BotStateService.ORDER
    if query['data'] == '1':
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="برای دریافت مشاوره به آیدی زیر پیام دهید")
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="%s" %Experts.objects.all()[0].telegram_id)
        keyboard = [[telegram.KeyboardButton(text='/start')]]
        inline_keyboard = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="*لطفا برای شروع مجدد گزینه‌ی زیر را لمس کنید:*",
                        reply_markup=inline_keyboard,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return ConversationHandler.END


def select_item(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from baghcheban.models import Utilities, Fertilizer, Flower, Seed
    query = update.callback_query
    try:
        shop_type = user_data['choice']
    except KeyError:
        shop_type = query['data']
    if shop_type == 'Flower':
        items = Flower.objects.filter(availability=True)
    elif shop_type == 'Fertilizer':
        items = Fertilizer.objects.filter(availability=True)
    elif shop_type == 'Utilities':
        items = Utilities.objects.filter(availability=True)
    elif shop_type == 'Seed':
        items = Seed.objects.filter(availability=True)
    for item in items:
        user_data['%s' %item.name] = 0
        inline_keyboard = telegram.InlineKeyboardMarkup(
            [[telegram.InlineKeyboardButton(text='-', callback_data='-1'),
              telegram.InlineKeyboardButton(text='0', callback_data='num'),
              telegram.InlineKeyboardButton(text='+', callback_data='+1')]]
            , resize_keyboard=True)
        bot.send_photo(chat_id=query['message']['chat']['id'], photo=item.image)
        bot.sendMessage(chat_id=query['message']['chat']['id'],
                        text="* %s ------------- %i تومن *" % (item.name, item.price),
                        reply_markup=inline_keyboard, parse_mode=telegram.ParseMode.MARKDOWN)
    inline_keyboard_confirm = telegram.InlineKeyboardMarkup(
        [[telegram.InlineKeyboardButton(text='خرید گل و گیاه', callback_data='Flower')],
         [telegram.InlineKeyboardButton(text='خرید کود و سم', callback_data='Fertilizer')],
         [telegram.InlineKeyboardButton(text='خرید بذر', callback_data='Seed')],
         [telegram.InlineKeyboardButton(text='خرید لوازم و تجهیزات', callback_data='Utilities')],
         [telegram.InlineKeyboardButton(text='تایید', callback_data='ok')],
         [telegram.InlineKeyboardButton(text='انصراف', callback_data='cancel')]], resize_keyboard=True)
    bot.sendMessage(chat_id=query['message']['chat']['id'], text='*لطفا گزینه‌ی مورد نظر را انتخاب کنید*',
                    parse_mode=telegram.ParseMode.MARKDOWN, reply_markup=inline_keyboard_confirm)
    return BotStateService.EDIT


def add_or_remove_item(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    user = user_data['user']
    query = update.callback_query
    inline_message_id = query.message.message_id
    item = query.message.text.split('-------------')[0].strip()
    inline_chat_id = query.message.chat_id
    if item in user_data:
        quantity = user_data['%s' %item]
    if query['data'] == '+1':
        quantity += 1
        user_data[item] = quantity
        inline_keyboard = telegram.InlineKeyboardMarkup(
            [[telegram.InlineKeyboardButton(text='-', callback_data='-1'),
              telegram.InlineKeyboardButton(text='%i' %quantity, callback_data='num'),
              telegram.InlineKeyboardButton(text='+', callback_data='+1')]])
        bot.editMessageReplyMarkup(message_id=inline_message_id, chat_id=inline_chat_id,
                                   reply_markup=inline_keyboard)
        bot.answerCallbackQuery(callback_query_id=query.id, text='با موفقیت اضافه شد', show_alert=False)
    if query['data'] == '-1':
        if quantity != 0:
            quantity -= 1
            user_data['%s' %item] = quantity
            inline_keyboard = telegram.InlineKeyboardMarkup(
                [[telegram.InlineKeyboardButton(text='-', callback_data='-1'),
                  telegram.InlineKeyboardButton(text='%i' %quantity, callback_data='num'),
                  telegram.InlineKeyboardButton(text='+', callback_data='+1')]])
            bot.editMessageReplyMarkup(message_id=inline_message_id, chat_id=inline_chat_id,
                                       reply_markup=inline_keyboard)
            bot.answerCallbackQuery(callback_query_id=query.id, text='با موفقیت حذف شد', show_alert=False)
    elif query['data'] == 'ok':
        bot.sendMessage(chat_id=query['message']['chat']['id'],
                        text='*لطفا آدرس کامل خود را وارد کنید*'
                        , parse_mode=telegram.ParseMode.MARKDOWN)
        return BotStateService.CONFIRM

    elif query['data'] == 'cancel':
        keyboard = [[telegram.KeyboardButton(text='/start')]]
        inline_keyboard = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="*لطفا برای شروع مجدد گزینه‌ی زیر را لمس کنید:*",
                        reply_markup=inline_keyboard,
                        parse_mode=telegram.ParseMode.MARKDOWN)

        return ConversationHandler.END
    elif query['data'] == 'Flower' or query['data'] == 'Fertilizer' or query['data'] == 'Utilities' \
            or query['data'] == 'Seed':
        user_data['choice'] = query['data']
        return select_item(bot, update, user_data)


def confirm(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from baghcheban.models import Invoice, Order, Flower, Fertilizer, Seed, Utilities
    user = user_data['user']
    invoice = Invoice()
    invoice.user = user
    invoice.address = update.message.text
    invoice.save()
    summation = 0
    for item in user_data:
        if item != 'user' and item != 'invoice' and item != 'choice' and user_data[item] != 0:
            order = Order()
            try:
                model = Flower.objects.get(name=item)
            except:
                try:
                    model = Utilities.objects.get(name=item)
                except:
                    try:
                        model = Seed.objects.get(name=item)
                    except:
                        try:
                            model = Fertilizer.objects.get(name=item)
                        except:
                            ConversationHandler.END
            order.item = model
            order.quantity = user_data[item]
            summation += order.item.price * order.quantity
            order.invoice = invoice
            order.save()

    invoice.amount = summation
    invoice.save()
    user_data['invoice'] = invoice
    bot.sendMessage(chat_id=update.message.chat_id,
                    text="*لطفا نام و نام خانوادگی خود را وارد کنید:*",
                    parse_mode=telegram.ParseMode.MARKDOWN)

    return BotStateService.NAME


def get_name(bot, update, user_data):
    user_data['user'].name = update.message.text
    user_data['user'].save()
    reply_markup = telegram.InlineKeyboardMarkup(
        [[telegram.InlineKeyboardButton(text='پرداخت اعتباری', callback_data='پرداخت اعتباری')]])
    bot.sendMessage(chat_id=update.message.chat_id,
                    text="*لطفا نحوه‌ی پرداختتان را انتخاب کنید:*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return BotStateService.PAYMENT


def payment(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from baghcheban.views import generate_payment_link
    user = user_data['user']
    invoice = user_data['invoice']
    query = update.callback_query
    if query['data'] == 'پرداخت اعتباری':
        link, error = generate_payment_link(user, invoice, 'telegram bot order')
        if not error:
            text = '*لینک زیر رو انتخاب کنید تا به صفحه‌ی پرداخت برید*' + '\n' + link
            bot.sendMessage(chat_id=user.chat_id,
                            text=text,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            reply_markup = telegram.InlineKeyboardMarkup(
                [[telegram.InlineKeyboardButton(text='پرداخت کردم', callback_data='پرداخت کردم')],
                 [telegram.InlineKeyboardButton(text='انصراف', callback_data='انصراف')]])
            bot.sendMessage(chat_id=user.chat_id,
                            text="*لطفا بعد از پرداخت گزینه‌ی پرداخت کردم رو انتخاب کنید:*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            return BotStateService.PAYMENT_CONFIRM

        else:
            text = '*سایت پرداخت در دسترس نیست لطفا بعدا دوباره تلاش کنید*'
            bot.sendMessage(chat_id=user.chat_id,
                            text=text,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return BotStateService.PAYMENT


def payment_confirm(bot, update, user_data):
    user = user_data['user']
    invoice = user_data['invoice']
    query = update.callback_query
    if query['data'] == 'پرداخت کردم':
        if invoice.is_paid:
            bot.sendMessage(chat_id=user.chat_id,
                            text="*سفارشتون تا ۴۸ ساعت آینده به دستتون می‌رسه منتظر ما باشید*",
                            parse_mode=telegram.ParseMode.MARKDOWN)
            keyboard = [[telegram.KeyboardButton(text='/start')]]
            inline_keyboard = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
            bot.sendMessage(chat_id=user_data['user'].chat_id,
                            text="*لطفا برای شروع مجدد گزینه‌ی زیر را لمس کنید:*",
                            reply_markup=inline_keyboard,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return ConversationHandler.END
        else:
            bot.sendMessage(chat_id=user.chat_id,
                            text="*پرداختتون موفقیت آمیز نبوده دوباره تلاش کنید*",
                            parse_mode=telegram.ParseMode.MARKDOWN)
    if query['data'] == 'انصراف' and invoice.is_paid is False:
        invoice.delete()
        user = user_data['user']
        keyboard = [[telegram.KeyboardButton(text='/start')]]
        inline_keyboard = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="*لطفا برای شروع مجدد گزینه‌ی زیر را لمس کنید:*",
                        reply_markup=inline_keyboard,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return ConversationHandler.END

    if query['data'] == 'انصراف' and invoice.is_paid is True:
        user = user_data['user']
        bot.sendMessage(chat_id=user.chat_id,
                        text="*مبلغ سفارشتون پرداخت شده و دیگه قابلیت انصراف وجود نداره*",
                        parse_mode=telegram.ParseMode.MARKDOWN)
        bot.sendMessage(chat_id=user.chat_id,
                        text="*سفارشتون تا ۴۸ ساعت آینده به دستتون می‌رسه منتظر ما باشید*",
                        parse_mode=telegram.ParseMode.MARKDOWN)
        keyboard = [[telegram.KeyboardButton(text='/start')]]
        inline_keyboard = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
        bot.sendMessage(chat_id=user_data['user'].chat_id,
                        text="*لطفا برای شروع مجدد گزینه‌ی زیر را لمس کنید:*",
                        reply_markup=inline_keyboard,
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return ConversationHandler.END


def invalid(bot, update):
    keyboard = [[telegram.KeyboardButton(text='/start')]]
    inline_keyboard = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    update.message.reply_text('دستور شما یافت نشد برای شروع مجدد گزینه‌ی شروع را لمس کنید',
                              reply_markup=inline_keyboard)
    return ConversationHandler.END
