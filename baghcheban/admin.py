from django.contrib import admin
from .models import User, Invoice, Experts, Fertilizer, Flower, Order, Seed, Utilities
from django.contrib.auth.admin import UserAdmin


class InvoiceAdmin(admin.ModelAdmin):
    list_display = ['user', 'amount', 'is_active', 'is_paid', 'is_done']


admin.site.register(User)
admin.site.register(Experts)
admin.site.register(Seed)
admin.site.register(Flower)
admin.site.register(Fertilizer)
admin.site.register(Utilities)
admin.site.register(Order)
admin.site.register(Invoice, InvoiceAdmin)
