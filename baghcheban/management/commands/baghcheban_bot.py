from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        import logging

        from baghcheban.bot import payment_confirm, invalid, get_name, BotStateService, confirm, add_or_remove_item, \
            payment, select_type, select_item, start, get_phone
        from telegram.ext import Updater, CommandHandler, Filters, MessageHandler, ConversationHandler, \
            CallbackQueryHandler

        bot_token = '766901525:AAGa5bQZL-K2_9kfxpKsRUwNm-Qkwdztfl8'
        updater = Updater(token=bot_token)
        dispatcher = updater.dispatcher
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)

        modify_handler = ConversationHandler(
            entry_points=[CommandHandler('start', callback=start, pass_user_data=True)],
            states={BotStateService.GETPHONE: [MessageHandler(Filters.contact, get_phone, pass_user_data=True)],
                    BotStateService.PAYMENT: [CallbackQueryHandler(payment, pass_user_data=True)],
                    BotStateService.PAYMENT_CONFIRM: [CallbackQueryHandler(payment_confirm, pass_user_data=True)],
                    BotStateService.ORDER: [CallbackQueryHandler(select_item, pass_user_data=True)],
                    BotStateService.SELECTSERVICE: [CallbackQueryHandler(select_type, pass_user_data=True)],
                    BotStateService.EDIT: [CallbackQueryHandler(add_or_remove_item, pass_user_data=True)],
                    BotStateService.CONFIRM:[CallbackQueryHandler(confirm, pass_user_data=True)],
                    BotStateService.NAME: [CallbackQueryHandler(get_name, pass_user_data=True)]},
            fallbacks=[MessageHandler(Filters.text & Filters.command, callback=invalid)]
        )

        dispatcher.add_handler(modify_handler)
        updater.start_polling()
